# fits-viewer

> Fits file format image viewer.

This is a streaming fits file loader, intended to allow processing while
data is loaded. The example applicaiton creates the image from 1 or 3
axis fits images as the data chunks are read in.

## Live example

A live example is available at https://johnwebbcole.gitlab.io/fits-viewer/
