import Debug from "debug";
const debug = Debug("fits2image");

const noop = value => value;

export const drawFitsData = function(
  context,
  fitsinfo,
  planes,
  scale = noop,
  colors
) {
  var start = performance.now();
  //   colors = colors || fitsinfo.NAXIS > 2 ? [0, 1, 2] : [0, 0, 0];
  colors = colors || [0, 0, 0];
  debug("drawFitsData.start", colors, fitsinfo.NAXIS);

  var imageData = context.getImageData(0, 0, fitsinfo.NAXIS1, fitsinfo.NAXIS2);

  var pixel;
  colors.forEach((planeIndex, i) => {
    pixel = i;

    planes[planeIndex].forEach(value => {
      imageData.data[pixel] = scale(value);
      if (i === 0) imageData.data[pixel + 3] = 255;
      pixel += 4;
    });
  });

  debug(
    "drawFitsData.done",
    performance.now() - start,
    pixel,
    pixel / 4,
    imageData
  );

  context.putImageData(imageData, 0, 0);
};
