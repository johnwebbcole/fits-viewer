export const range = function(size) {
  return Array(size)
    .fill(0)
    .map((v, i) => i);
};
