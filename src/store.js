import Vue from "vue";
import Vuex from "vuex";
import { ArrayWindow } from "@/components/ArrayWindowStore";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    ArrayWindow
  }
});
