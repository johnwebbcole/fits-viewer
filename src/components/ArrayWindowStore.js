export const ArrayWindow = {
  namespaced: true,
  state: { x: 0, y: 0 },
  mutations: {
    setZoom(state, { x, y }) {
      state.x = x;
      state.y = y;
    },

    setZoomX(state, x) {
      state.x = x;
    },

    setZoomY(state, y) {
      state.y = y;
    }
  },
  getters: {
    getZoom({ x, y }) {
      return { x, y };
    }
  }
};
