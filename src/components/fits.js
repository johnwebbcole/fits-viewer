import Debug from "debug";
const debug = Debug("fits");

var minval, maxval;

var textDecoder = new TextDecoder("utf-8");
const lineToCard = line => {
  var parseValue = v => {
    var isString = v.slice(10, 11) === "'";
    if (isString) {
      var stop = v.slice(11).indexOf("'") + 11;
      return {
        value: v.slice(11, stop),
        comment: v.slice(stop)
      };
    } else
      return {
        value: v.slice(10, 30),
        comment: v.slice(30)
      };
  };

  var parse = s => {
    var isValue = s.slice(8, 9) === "=";
    return isValue ? { ...parseValue(s) } : { value: s.slice(10) };
  };

  return {
    key: line.slice(0, 8).trim(),
    ...parse(line)
  };
};

const cardType = {
  BITPIX: v => parseInt(v),
  BZERO: v => parseFloat(v),
  BSCALE: v => parseFloat(v),
  DATAMIN: v => parseFloat(v),
  DATAMAX: v => parseFloat(v),
  "MIPS-HI": v => parseInt(v),
  "MIPS-LO": v => parseInt(v)
};

const parseCards = card => {
  if (cardType[card.key]) card.value = cardType[card.key](card.value);
  else if (card.key.startsWith("NAXIS")) card.value = parseInt(card.value);
  else if (card.value === "                   T") card.value = true;
  else if (card.value === "                   F") card.value = false;
  return card;
};

const minmax = value => {
  if (value < minval) minval = value;
  if (value > maxval) maxval = value;
};

const types = {
  "8": Uint8Array,
  "16": Uint16Array,
  "32": Uint32Array,
  "-32": Float32Array
};

const getters = {
  "8": (dv, offset) => {
    var val = dv.getUint8(offset);

    minmax(val);
    return val;
  },
  "16": (dv, offset, fitsinfo) => {
    var val = dv.getInt16(offset, false);
    var value = val + fitsinfo._BZEROTRUNC;

    if (isNaN(value)) {
      console.warn(
        "getter returned NaN",
        `offset: ${offset} val: ${val} value: ${value}`
      );
      throw new Error("getter 16 created NaN");
    }
    minmax(value);
    return value;
  },
  "32": (dv, offset, fitsinfo) => {
    var val = dv.getInt32(offset, false);
    var value = val + fitsinfo._BZEROTRUNC;

    if (isNaN(value)) {
      console.warn(
        "getter returned NaN",
        `offset: ${offset} val: ${val} value: ${value}`
      );
      throw new Error("getter 16 created NaN");
    }
    minmax(value);
    return value;
  },
  "-32": (dv, offset, fitsinfo) => {
    var val = dv.getFloat32(offset, false);
    var value = val + fitsinfo._BZEROTRUNC;

    if (isNaN(value)) {
      console.warn(
        "getter returned NaN",
        `offset: ${offset} val: ${val} value: ${value} bzerotrunc: ${fitsinfo._BZEROTRUNC}`
      );
      throw new Error("getter '-32' created NaN");
    }
    minmax(value);

    return value;
  }
};

const parseHeader = function(value) {
  var header = [],
    offset = 0;
  // Read 2880 bytes until the `END` card is found.
  while (header.filter(c => c.startsWith("END")).length === 0) {
    if (offset + 2880 > value.length)
      console.warn(
        "offset is too far",
        offset,
        value.length,
        value,
        "headers that span chunks is not supported yet.  Try reloading."
      );
    // splice into the header array at he end
    var lines = textDecoder
      .decode(value.slice(offset, offset + 2880))
      .match(/.{80}/g);

    if (!lines) {
      console.warn("lines is empty or null", lines, offset, value);
      throw new Error("header error, textDecoder returned no lines");
    } else {
      header.splice(-1, 0, ...lines);
    }

    offset += 2880;
  }
  // debug("header", header);
  var cards = header.map(lineToCard).map(parseCards);
  var fitsinfo = cards.reduce((a, v) => {
    if (v.key) {
      if (a[v.key]) {
        if (Array.isArray(a[v.key])) a[v.key].push(v.value);
        else {
          a[v.key] = [a[v.key], v.value];
        }
      } else {
        a[v.key] = v.value;
      }
    }
    return a;
  }, {});

  /**
   * Add internal variables
   */
  fitsinfo._BZEROTRUNC = Math.round(fitsinfo.BZERO || 0);
  fitsinfo._headerOffset = offset;
  fitsinfo._BYTEPIX = Math.abs(fitsinfo.BITPIX) / 8;
  fitsinfo._pixels = Array.from("0".repeat(fitsinfo.NAXIS)).reduce(
    (a, v, i) => {
      return i === 0
        ? fitsinfo[`NAXIS${i + 1}`]
        : a * fitsinfo[`NAXIS${i + 1}`];
    },
    0
  );
  fitsinfo._size = fitsinfo._pixels * fitsinfo._BYTEPIX;
  fitsinfo._planePixels = fitsinfo.NAXIS1 * fitsinfo.NAXIS2;
  fitsinfo._planeSize = fitsinfo.NAXIS1 * fitsinfo.NAXIS2 * fitsinfo._BYTEPIX;
  if (!getters[fitsinfo.BITPIX])
    console.error("no getter", fitsinfo.BITPIX, getters);
  fitsinfo._getter = getters[fitsinfo.BITPIX];

  if (fitsinfo.DATAMAX && fitsinfo.DATAMIN) {
    // https://gist.github.com/vectorsize/7031902
    var istart = fitsinfo.DATAMAX,
      istop = fitsinfo.DATAMIN,
      ostart = 255,
      ostop = 0;
    fitsinfo._linear = value => {
      return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
    };
  } else {
    fitsinfo._linear = value => value;
  }
  // debug("parseHeader", { cards, fitsinfo, offset });
  return { cards, fitsinfo, offset };
};

function getByteArray(value, partial, offset, bytepix) {
  var bytearray, dangle;
  if (partial) {
    // @see https://stackoverflow.com/questions/14071463/how-can-i-merge-typedarrays-in-javascript
    var combined = new Uint8Array(partial.length + value.length);
    combined.set(partial);
    combined.set(value, partial.length);
    partial = undefined;
    dangle = combined.length % bytepix;
    bytearray = dangle === 0 ? combined : combined.slice(0, -dangle);
  } else {
    dangle = (value.length - offset) % bytepix;
    bytearray =
      offset === 0
        ? dangle === 0
          ? value
          : value.slice(offset, -dangle)
        : dangle === 0
        ? value.slice(offset)
        : value.slice(offset, -dangle);
  }
  return { bytearray, dangle };
}

export const parse = async function({ reader, resourceSize }, options = {}) {
  var cards, fitsinfo, offset;
  var partial = undefined;
  var processedCount = 0;
  var values = [];
  var position = 0;

  const read = async (reader, totalChunkSize = 0, chunkCount = 0) => {
    const result = await reader.read();
    const { value, done } = result;
    // debug("read chunk value", done, value, result);

    /**
     * Grab the header in 2880 byte sections
     * and parse the header if this is `chunkCount` 0.
     */
    if (chunkCount === 0) {
      ({ cards, fitsinfo, offset } = parseHeader(value));
      debug("header", { cards, fitsinfo, offset });
      if (options.onHeader) options.onHeader({ cards, fitsinfo, offset });
      // rawdata = new Uint8Array(fitsinfo._size);
    } else {
      offset = 0;
    }

    if (!value) debug("no value");
    if (value) {
      values.push({ buffer: offset ? value.slice(offset) : value, position });
      position += value.byteLength - offset;

      if (options.data) {
        var { bytearray, dangle } = getByteArray(
          value,
          partial,
          offset,
          fitsinfo._BYTEPIX
        );

        partial = dangle && value.slice(dangle * -1);

        var datachunk = new DataView(bytearray.buffer);
        processedCount += options.data(
          { data: datachunk, offset, getter: fitsinfo._getter, chunkCount },
          { cards, fitsinfo, offset }
        );
      }
    }

    /**
     * continue to the next chunk
     */
    if (done) {
      debug("chunks processed during data events:", processedCount);

      if (fitsinfo._size > 0) {
        /**
         * Combine each `values` array into a single combined array
         * to make it easier to cross array boudaries.
         */
        var combinedvalues = new Uint8Array(fitsinfo._size);

        values.forEach(({ buffer, position }) => {
          if (position > combinedvalues.length) return;
          var remaining = combinedvalues.length - position;
          if (buffer.length > remaining) var limit = remaining;
          combinedvalues.set(limit ? buffer.slice(0, limit) : buffer, position);
        });

        /**
         * Convert the combined values array into a data array matching
         * the typed array of the input data.
         */
        var dv = new DataView(combinedvalues.buffer);
        var data = new types[fitsinfo.BITPIX](fitsinfo._pixels);
        var getter = getters[fitsinfo.BITPIX];

        var j = 0;
        for (var i = 0; i < data.length; i++) {
          data[i] = getter(dv, j, fitsinfo);
          j += fitsinfo._BYTEPIX;
        }
      }

      if (options.done) options.done({ chunkCount, resourceSize });
      return {
        chunkCount,
        minval,
        maxval,
        fitsinfo,
        data
      };
    }

    const runningTotal = totalChunkSize + value.length;
    // eslint-disable-next-line
    const percentComplete = Math.round((runningTotal / resourceSize) * 100);

    const progress = `${percentComplete}% (chunk ${chunkCount})`;

    if (options.progress)
      options.progress({
        progress,
        runningTotal,
        resourceSize,
        percentComplete
      });

    return read(reader, runningTotal, chunkCount + 1);
  };

  return read(reader);
};

/**
 * Open a reader stream given a url
 * @function open
 * @param  {String} url A url to open using `fetch` api
 * @return {Object} Object with the response and stream.
 */
export const open = async function(url) {
  minval = Number.MAX_VALUE;
  maxval = Number.MIN_VALUE;

  const response = await fetch(url);
  debug("open response", response);
  const reader = response.body.getReader();
  const contentLengthHeader = response.headers.get("Content-Length");
  const resourceSize = parseInt(contentLengthHeader, 10);
  debug("response", response);
  debug("contentLengthHeader", contentLengthHeader);
  debug("resourceSize", resourceSize, resourceSize % 2880);
  return {
    response,
    reader,
    contentLengthHeader,
    resourceSize
  };
};

const getHistogram = function(array) {
  // debug("getHistsogram", array);
  return Object.entries(
    array.reduce((hist, value) => {
      if (!hist[value]) hist[value] = 0;
      hist[value]++;
      return hist;
    }, {})
  )
    .map(e => {
      return {
        value: Number.parseInt(e[0]),
        count: e[1]
      };
    })
    .sort((a, b) => {
      return a.value - b.value;
    });
};

// function flipPlane(data, fitsinfo) {
//   var plane = new types[fitsinfo.BITPIX](data.length);
//   for (var y = 0; y < fitsinfo.NAXIS2; y++) {
//     var offset = (fitsinfo.NAXIS2 - y) * fitsinfo.NAXIS1;
//     var flipped = y * fitsinfo.NAXIS1;
//     var line = data.slice(offset, offset + fitsinfo.NAXIS2);
//     plane.set(line, flipped);
//   }
//   return plane;
// }

function getPlane(data, index, fitsinfo, flip = true) {
  var plane = new types[fitsinfo.BITPIX](fitsinfo._planePixels);
  if (!flip) {
    plane = data.slice(
      fitsinfo._planePixels * index,
      fitsinfo._planePixels * (index + 1)
    );
  } else {
    var planeStart = index * fitsinfo.NAXIS1 * fitsinfo.NAXIS2;
    for (var y = 0; y < fitsinfo.NAXIS2; y++) {
      var offset = planeStart + (fitsinfo.NAXIS2 - y) * fitsinfo.NAXIS1;
      var flipped = y * fitsinfo.NAXIS1;
      var line = data.slice(offset, offset + fitsinfo.NAXIS2);
      plane.set(line, flipped);
    }
  }

  return plane;
}

export const rawToPlanes = function(fitsinfo, data) {
  var planes = new Array(fitsinfo.NAXIS);
  planes.fill(new types[fitsinfo.BITPIX](fitsinfo._planePixels));
  for (var i = 0; i < planes.length; i++) {
    planes[i] = getPlane(data, i, fitsinfo);
  }
  return planes;
};

function linearScale(min, max) {
  // https://gist.github.com/vectorsize/7031902
  var ostart = 255,
    ostop = 0;
  return value => {
    return ostart + (ostop - ostart) * ((value - max) / (min - max));
  };
}

export default {
  open,
  parse,
  rawToPlanes,
  getHistogram,
  linearScale
};
